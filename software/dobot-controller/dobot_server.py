# -*- coding: UTF-8 -*-
#!/bin/python3
from serial.tools import list_ports
from pydobot import Dobot
import orden
import re
import threading
import time
import zmq


# Init socket
context = zmq.Context()
socket = context.socket(zmq.REP)
notify = context.socket(zmq.PUB)
socket.bind("tcp://127.0.0.1:5555")
notify.bind("tcp://127.0.0.1:5556")

#Switch de las tareas HARIA FALTA ESTE SWITCH
# def switch(case,device,orden):
#     #def switch(case):
#     global salir
    
#     if case==1:
#         hilo_tarea = threading.Thread(target=tarea_1)
#         hilo_tarea.start()
#     elif case==2:
#         hilo_tarea = threading.Thread(target=tarea_2)
#         hilo_tarea.start()
#     elif case==3:
#         hilo_tarea = threading.Thread(target=tarea_3)
#         hilo_tarea.start()
#     elif case==4:
#         hilo_tarea = threading.Thread(target=tarea_4)
#         hilo_tarea.start()        
#     elif case==5:
#         hilo_tarea = threading.Thread(target=tarea_5)
#         hilo_tarea.start()        
#     elif case==6:
#         hilo_tarea = threading.Thread(target=tarea_6)
#         hilo_tarea.start()
#     elif case==7:
#         hilo_tarea = threading.Thread(target=tarea_7)
#         hilo_tarea.start()
#     elif case==8:
#         hilo_tarea = threading.Thread(target=tarea_8)
#         hilo_tarea.start()
#     elif case==9:
#         hilo_tarea = threading.Thread(target=tarea_9)
#         hilo_tarea.start()
#     elif case==10:
#         hilo_tarea = threading.Thread(target=tarea_10)
#         hilo_tarea.start()    
#     elif case==11:
#         hilo_tarea = threading.Thread(target=tarea_11)
#         hilo_tarea.start()
#     elif case==12:
#         hilo_tarea = threading.Thread(target=tarea_12)
#         hilo_tarea.start()
#     else:
#         print("Opción inválida!")

#Tarea de devolver siempre parámetros de velocidad y posición
def hilo_parametros(device):
    global salir
    t = 0
    dt = 0.1 #Muestreo del tiempo
    while (salir==0):
        try:
            pose = device.pose()
            v_a = device.get_v_a()
            pose = ','.join(map(str, pose))
            v_a = ','.join(map(str, v_a))
            notify.send_string('pose %s,%s,%s' % (t, pose, v_a))
            time.sleep(dt)
            t = t + dt
        except:
            pass

# The actual tasks
def task_home(device):
    print("\nMoviendo Dobot a posición cero...\n")
    device.home()
    print("\nRetorno del home.\n")
def task_position(device):
    print('\nDobot Position:\n')
    (x, y, z, r, j1, j2, j3, j4) = device.pose()
    print(f' x : {x}\n y : {y}\n z : {z}\n j1 : {j1}\n j2 : {j2}\n j3 : {j3}\n j4 : {j4}\n')
def task_speed_joint(device):
    print('\nDobot Speed Joint:\n')
    (x, y, z, r, j1, j2, j3, j4) = device.get_v_a()
    print(f' v_j1 : {x}\n v_j2 : {y}\n v_j3 : {z}\n v_j4 : {r}\n a_j1 : {j1}\n a_j2 : {j2}\n a_j3 : {j3}\n a_j4 : {j4}\n')
def task_change_speed_jog(device,v,a):
#    v = int(input("\nValor de la velocidad: \n"))
#    time.sleep(2)
#    a = int(input("\nValor de la aceleración: \n"))
#    time.sleep(2)
    device.speed_jog(v,a)
    print("\nCambio exitoso!\n")
def task_change_speed_ptp(device,v,a):
#    v = int(input("\nValor de la velocidad: \n"))
#    time.sleep(2)
#    a = int(input("\nValor de la aceleración: \n"))
#    time.sleep(2)
    device.speed_ptp(v,a)
    print("\nCambio exitoso!\n")
def task_move_to_angle(device,ang,art):
#    ang = int(input("\nÁngulo: \n"))
#    time.sleep(2)
#    art = int(input("\nArticulación: \n"))
#    time.sleep(2)
    print('\nMoviendo a %sº la articulación %s.\n' % (ang,art))
    orden.move_angle(ang,art,device)
def task_move_to_point_angle(device,j1,j2,j3,j4,mode):
#     j1 = int(input("J1 : "))
#     j2 = int(input("J2 : "))
#     j3 = int(input("J3 : "))
#     j4 = int(input("J4 : "))
#     mode = int(input("MODE (LINEAR/JOG) : "))
    print('\nMoviendo a la posición (%s,%s,%s;%s)º.\n' % (j1,j2,j3,j4,))
    device.move_to_point_angle(j1, j2, j3, j4, mode, wait=False)
def task_move_to_point_xyz(device,x,y,z,r,mode):
#     x = int(input("X : "))
#     y = int(input("Y : "))
#     z = int(input("Z : "))
#     r = int(input("r : "))
#     mode = int(input("MODE (LINEAR/JOG) : "))
    print('\nMoviendo a la posición (%s,%s,%s;%s)º.\n' % (x,y,z,r,))
    device.move_to_point_xyz(x, y, z, r, mode, wait=False)
def task_increase_angle(device,j1,j2,j3,j4):
#     j1 = int(input("J1 : "))
#     j2 = int(input("J2 : "))
#     j3 = int(input("J3 : "))
#     j4 = int(input("J4 : "))
    device.increase_angle(j1, j2, j3, j4, wait=False)
    print("\nMoviendo Dobot...\n")
def task_increase_xyz(device,x,y,z,r,mode):
#     x = int(input("X : "))
#     y = int(input("Y : "))
#     z = int(input("Z : "))
#     r = int(input("r : "))
#     mode = int(input("MODE (LINEAR/JOG) : "))
    device.increase_xyz(x, y, z, r, mode, wait=False)
    print("\nMoviendo Dobot...\n")
def task_wait(device,t):
#    t = int(input("\nTiempo (ms) : "))
#    time.sleep(2)
    device.wait(t)
    print('\nEsperando %s ms.\n'% (t))
def task_move_joint(device,art):
#    art = int(input("\nArticulación: "))
#    time.sleep(2)
    print("\nMoviendo la articulación %s... (Presione 'p' para deterner)\n" % (art))
    device.move_joint(art, wait=False)
#     if((input(""))=='p'):
#         device.movejoint(0, wait=False)
#         print("\nDetenido!\n")
def task_move_coordinate(device,c):
#    art = int(input("\nArticulación: "))
#    time.sleep(2)
    print("\nMoviendo la coordenada %s... (Presione 'p' para deterner)\n" % (c))
    device.move_coordinate(c, wait=False)
#     if((input(""))=='p'):
#         device.move_coordinate(0, wait=False)
#         print("\nDetenido!\n")
def task_grip(device,enable,option):
    #print("\nGrip activado\n")
    device.grip(enable,option)
#    device.suck(option)
def task_stop(device):
    device.stop()
    print("\nParada rápida del Dobot!\n")
    device.start_dobot()
def task_exit(device):
    global salir
    salir=1
    print("\nSaliendo!\n")
    #

# Parse command string ('<name>: [1, ...]') and extract elements. 
def parse_string(s):
    try:
        v = re.match('(.*):\s*\[(.*)\]', s).groups()
        name = v[0]
        values = v[1].split(',')
        id = int(values[0])
        args = [float(a) for a in values[1:]]
        return {
            'task_name': name,
            'task_id': id,
            'task_args': args,
        }
    except:
        print('[Error] Invalid format')
        return None

# Map actions by id
TASKS = [
    task_exit,
    task_home,
    task_position,
    task_speed_joint,
    task_change_speed_jog,
    task_change_speed_ptp,
    task_move_to_angle,
    task_move_to_point_angle,
    task_move_to_point_xyz,
    task_increase_angle,
    task_increase_xyz,
    task_wait,
    task_move_joint,
    task_move_coordinate,
    task_grip,
    task_stop,
    task_exit
]

def init_dobot():
    # Conexión al robot
    port = list_ports.comports()[0].device
    device = Dobot(port=port, verbose=False)
    return device

if __name__ == "__main__":
    global salir
    salir = False
    # Wait for commands
    device = init_dobot()
    print("Dobot is listening\n")
    #  Wait for next request from client
    hilo_tarea = threading.Thread(target=hilo_parametros, args=([device]))
    hilo_tarea.start()
    while not salir:
        message = socket.recv_string()
        print("Received request: %s" % message)
        command = parse_string(message)
        try:
            name = command['task_name']
            id = command['task_id']
            args = command['task_args']
            if name == 'config':
                if id == 0:
                    task_exit(device)
            elif name == 'action':
                task = TASKS[id]
                hilo_tarea = threading.Thread(target=task, args=([device] + args))
                hilo_tarea.start()
            print("Action Completed.\n")
        except:
            print('[Error] Invalid command.\n')
        socket.send_string(message)

  
