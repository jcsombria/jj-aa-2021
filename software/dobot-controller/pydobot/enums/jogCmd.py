from enum import Enum


class JOGCmd(Enum):
    """
    0. IDEL, ]
        Void
        
    1. AP_DOWN,
        X+/Joint1+
        
    2. AN_DOWN,
        X-/Joint1-
        
    3. BP_DOWN,
        Y+/Joint2+
        
    4. BN_DOWN,
        Y-/Joint2-
        
    5. CP_DOWN,
        Z+/Joint3+
        
    6. CN_DOWN,
        Z-/Joint3-
        
    7. DP_DOWN,
        R+/Joint4+
        
    8. DN_DOWN,
        R-/Joint4-
      
    """
    IDEL = 0x00
    AP_DOWN = 0x01
    AN_DOWN = 0x02
    BP_DOWN = 0x03
    BN_DOWN = 0x04
    CP_DOWN = 0x05
    CN_DOWN = 0x06
    DP_DOWN = 0x07
    DN_DOWN = 0x08
