import serial
import struct
import time
import threading
import warnings

from .message import Message
from .enums import PTPMode
from .enums import JOGCmd
from .enums.CommunicationProtocolIDs import CommunicationProtocolIDs
from .enums.ControlValues import ControlValues


class Dobot:

    def __init__(self, port, verbose=False):
        threading.Thread.__init__(self)

        self._on = True
        self.verbose = verbose
        self.lock = threading.Lock()
        self.ser = serial.Serial(port,
                                 baudrate=115200,
                                 parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE,
                                 bytesize=serial.EIGHTBITS)
        is_open = self.ser.isOpen()
        if self.verbose:
            print('pydobot: %s open' % self.ser.name if is_open else 'failed to open serial port')

        self._set_queued_cmd_start_exec()
        self._set_queued_cmd_clear()
        self._set_ptp_joint_params(200, 200, 200, 200, 200, 200, 200, 200)
        self._set_ptp_coordinate_params(velocity=200, acceleration=200)
        self._set_ptp_jump_params(10, 200)
        self._set_ptp_common_params(velocity=100, acceleration=100)
        self._get_pose()

    """
        Gets the current command index
    """
    def _get_queued_cmd_current_index(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.GET_QUEUED_CMD_CURRENT_INDEX
        response = self._send_command(msg)
        idx = struct.unpack_from('L', response.params, 0)[0]
        return idx

    """
        Gets the real-time pose of the Dobot
    """
    def _get_pose(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.GET_POSE
        response = self._send_command(msg)
        self.x = struct.unpack_from('f', response.params, 0)[0]
        self.y = struct.unpack_from('f', response.params, 4)[0]
        self.z = struct.unpack_from('f', response.params, 8)[0]
        self.r = struct.unpack_from('f', response.params, 12)[0]
        self.j1 = struct.unpack_from('f', response.params, 16)[0]
        self.j2 = struct.unpack_from('f', response.params, 20)[0]
        self.j3 = struct.unpack_from('f', response.params, 24)[0]
        self.j4 = struct.unpack_from('f', response.params, 28)[0]

        if self.verbose:
            print("pydobot: x:%03.1f \
                            y:%03.1f \
                            z:%03.1f \
                            r:%03.1f \
                            j1:%03.1f \
                            j2:%03.1f \
                            j3:%03.1f \
                            j4:%03.1f" %
                  (self.x, self.y, self.z, self.r, self.j1, self.j2, self.j3, self.j4))
        return response

    def _read_message(self):
        time.sleep(0.1)
        b = self.ser.read_all()
        if len(b) > 0:
            msg = Message(b)
            if self.verbose:
                print('pydobot: <<', msg)
            return msg
        return

    def _send_command(self, msg, wait=False):
        self.lock.acquire()
        self._send_message(msg)
        response = self._read_message()
        self.lock.release()

        if not wait:
            return response

        expected_idx = struct.unpack_from('L', response.params, 0)[0]
        if self.verbose:
            print('pydobot: waiting for command', expected_idx)

        while True:
            current_idx = self._get_queued_cmd_current_index()

            if current_idx != expected_idx:
                time.sleep(0.1)
                continue

            if self.verbose:
                print('pydobot: command %d executed' % current_idx)
            break

        return response

    def _send_message(self, msg):
        time.sleep(0.1)
        if self.verbose:
            print('pydobot: >>', msg)
        self.ser.write(msg.bytes())

    """
        Executes the CP Command
    """
    def _set_cp_cmd(self, x, y, z):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_CP_CMD
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray(bytes([0x01]))
        msg.params.extend(bytearray(struct.pack('f', x)))
        msg.params.extend(bytearray(struct.pack('f', y)))
        msg.params.extend(bytearray(struct.pack('f', z)))
        msg.params.append(0x00)
        return self._send_command(msg)

    """
        Sets the status of the gripper
    """
    def _set_end_effector_gripper(self, enable, option):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_END_EFFECTOR_GRIPPER
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        if enable==1:
            msg.params.extend(bytearray([0x01]))
        else:
            msg.params.extend(bytearray([0x00]))
        if option==1:
            msg.params.extend(bytearray([0x01]))
        else:
            msg.params.extend(bytearray([0x00]))
        return self._send_command(msg)

    """
        Sets the status of the suction cup
    """
    def _set_end_effector_suction_cup(self, enable, option):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_END_EFFECTOR_SUCTION_CUP
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        if enable==1:
            msg.params.extend(bytearray([0x01]))
        else:
            msg.params.extend(bytearray([0x00]))
        if option==1:
            msg.params.extend(bytearray([0x01]))
        else:
            msg.params.extend(bytearray([0x00]))
        return self._send_command(msg)

    """
        Sets the velocity ratio and the acceleration ratio in PTP mode
    """
    def _set_ptp_joint_params(self, v_x, v_y, v_z, v_r, a_x, a_y, a_z, a_r):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_PTP_JOINT_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', v_x)))
        msg.params.extend(bytearray(struct.pack('f', v_y)))
        msg.params.extend(bytearray(struct.pack('f', v_z)))
        msg.params.extend(bytearray(struct.pack('f', v_r)))
        msg.params.extend(bytearray(struct.pack('f', a_x)))
        msg.params.extend(bytearray(struct.pack('f', a_y)))
        msg.params.extend(bytearray(struct.pack('f', a_z)))
        msg.params.extend(bytearray(struct.pack('f', a_r)))
        return self._send_command(msg)

    """
        Sets the velocity and acceleration of the Cartesian coordinate axes in PTP mode
    """
    def _set_ptp_coordinate_params(self, velocity, acceleration):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_PTP_COORDINATE_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', velocity)))
        msg.params.extend(bytearray(struct.pack('f', velocity)))
        msg.params.extend(bytearray(struct.pack('f', acceleration)))
        msg.params.extend(bytearray(struct.pack('f', acceleration)))
        return self._send_command(msg)

    """
       Sets the lifting height and the maximum lifting height in JUMP mode
    """
    def _set_ptp_jump_params(self, jump, limit):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_PTP_JUMP_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', jump)))
        msg.params.extend(bytearray(struct.pack('f', limit)))
        return self._send_command(msg)


    """
        Sets the velocity ratio, acceleration ratio in PTP mode
    """
    def _set_ptp_common_params(self, velocity, acceleration):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_PTP_COMMON_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', velocity)))
        msg.params.extend(bytearray(struct.pack('f', acceleration)))
        return self._send_command(msg)

    """
        Executes PTP command
    """
    def _set_ptp_cmd(self, x, y, z, r, mode, wait):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_PTP_CMD
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray([mode.value]))
        msg.params.extend(bytearray(struct.pack('f', x)))
        msg.params.extend(bytearray(struct.pack('f', y)))
        msg.params.extend(bytearray(struct.pack('f', z)))
        msg.params.extend(bytearray(struct.pack('f', r)))
        return self._send_command(msg, wait)
    
    """
       Executes JOG command
    """
    def _set_jog_cmd(self, joint, mode, wait):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_JOG_CMD
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray([joint]))
        msg.params.extend(bytearray([mode.value]))
        return self._send_command(msg, wait)

    """
       Sets the velocity and the acceleration in JOG mode (joints coordinates)
    """
    def _set_jog_joint_params(self, v_j1, v_j2, v_j3, v_j4, a_j1, a_j2, a_j3, a_j4):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_JOG_JOINT_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', v_j1)))
        msg.params.extend(bytearray(struct.pack('f', v_j2)))
        msg.params.extend(bytearray(struct.pack('f', v_j3)))
        msg.params.extend(bytearray(struct.pack('f', v_j4)))
        msg.params.extend(bytearray(struct.pack('f', a_j1)))
        msg.params.extend(bytearray(struct.pack('f', a_j2)))
        msg.params.extend(bytearray(struct.pack('f', a_j3)))
        msg.params.extend(bytearray(struct.pack('f', a_j4)))
        return self._send_command(msg)

    """
       Sets the velocity and the acceleration in JOG mode (cartesians coordinates)
    """
    def _set_jog_coordinate_params(self, v_j1, v_j2, v_j3, v_j4, a_j1, a_j2, a_j3, a_j4):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_JOG_COORDINATE_PARAMS
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', v_j1)))
        msg.params.extend(bytearray(struct.pack('f', v_j2)))
        msg.params.extend(bytearray(struct.pack('f', v_j3)))
        msg.params.extend(bytearray(struct.pack('f', v_j4)))
        msg.params.extend(bytearray(struct.pack('f', a_j1)))
        msg.params.extend(bytearray(struct.pack('f', a_j2)))
        msg.params.extend(bytearray(struct.pack('f', a_j3)))
        msg.params.extend(bytearray(struct.pack('f', a_j4)))
        return self._send_command(msg)
    
    """
       Gets the velocity and the acceleration in JOG mode
    """
    def _get_jog_joint_params(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_JOG_JOINT_PARAMS
        msg.ctrl = ControlValues.ZERO
        response = self._send_command(msg)
        self.v_j1 = struct.unpack_from('f', response.params, 0)[0]
        self.v_j2 = struct.unpack_from('f', response.params, 4)[0]
        self.v_j3 = struct.unpack_from('f', response.params, 8)[0]
        self.v_j4 = struct.unpack_from('f', response.params, 12)[0]
        self.a_j1 = struct.unpack_from('f', response.params, 16)[0]
        self.a_j2 = struct.unpack_from('f', response.params, 20)[0]
        self.a_j3 = struct.unpack_from('f', response.params, 24)[0]
        self.a_j4 = struct.unpack_from('f', response.params, 28)[0]

        if self.verbose:
            print("pydobot: v_j1:%03.1f \
                            v_j2:%03.1f \
                            v_j3:%03.1f \
                            v_j4:%03.1f \
                            a_j1:%03.1f \
                            a_j2:%03.1f \
                            a_j3:%03.1f \
                            a_j4:%03.1f" %
                  (self.v_j1, self.v_j2, self.v_j3, self.v_j4, self.a_j1, self.a_j2, self.a_j3, self.a_j4))
        return response

    """
        Sets the velocity ratio, acceleration ratio in JOG mode
    """
    def _set_jog_common_params(self, velocity, acceleration):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_JOG_COMMON_PARAMS 
        msg.ctrl = ControlValues.THREE
        msg.params = bytearray([])
        msg.params.extend(bytearray(struct.pack('f', velocity)))
        msg.params.extend(bytearray(struct.pack('f', acceleration)))
        return self._send_command(msg)

    """
       Executes HOME command
    """
    def _set_home_cmd(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_HOME_CMD
        msg.ctrl = ControlValues.THREE
        return self._send_command(msg)
    
    """
        Clears command queue
    """
    def _set_queued_cmd_clear(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_QUEUED_CMD_CLEAR
        msg.ctrl = ControlValues.ONE
        return self._send_command(msg)

    """
        Start command
    """
    def _set_queued_cmd_start_exec(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_QUEUED_CMD_START_EXEC
        msg.ctrl = ControlValues.ONE
        return self._send_command(msg)

    """
        Wait command
    """
    def _set_wait_cmd(self, ms):
        msg = Message()
        msg.id = 110
        msg.ctrl = 0x03
        msg.params = bytearray(struct.pack('I', ms))
        return self._send_command(msg)

    """
        Stop command
    """
    def _set_queued_cmd_stop_exec(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_QUEUED_CMD_STOP_EXEC
        msg.ctrl = ControlValues.TWO
        return self._send_command(msg)
    
    def _set_queued_cmd_force_stop_exec(self):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_QUEUED_CMD_FORCE_STOP_EXEC
        msg.ctrl = ControlValues.ONE
        return self._send_command(msg)

    def _get_eio_level(self, address):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_EIO
        msg.ctrl = ControlValues.ZERO
        msg.params = bytearray([])
        msg.params.extend(bytearray([address]))
        return self._send_command(msg)

    def _set_eio_level(self, address, level):
        msg = Message()
        msg.id = CommunicationProtocolIDs.SET_GET_EIO
        msg.ctrl = ControlValues.ONE
        msg.params = bytearray([])
        msg.params.extend(bytearray([address]))
        msg.params.extend(bytearray([level]))
        return self._send_command(msg)

    def get_eio(self, addr):
        return self._get_eio_level(addr)

    def set_eio(self, addr, val):
        return self._set_eio_level(addr, val)

    def close(self):
        self._on = False
        self.lock.acquire()
        self.ser.close()
        if self.verbose:
            print('pydobot: %s closed' % self.ser.name)
        self.lock.release()

    def go(self, x, y, z, r=0.):
        warnings.warn('go() is deprecated, use move_to() instead')
        self.move_to(x, y, z, r)

    def move_to_point_angle(self, j1, j2, j3, j4, mod, wait=False):
        if mod == 0:
            self._set_ptp_cmd(j1, j2, j3, j4, mode=PTPMode.MOVJ_ANGLE, wait=wait)                    
        elif mod == 1:
            self._set_ptp_cmd(j1, j2, j3, j4, mode=PTPMode.MOVL_ANGLE, wait=wait)
        elif mod == 2:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.JUMP_ANGLE, wait=wait)
            
    def move_to_point_xyz(self, x, y, z, r, mod, wait=False):
        if mod == 0:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.MOVJ_XYZ, wait=wait)                    
        elif mod == 1:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.MOVL_XYZ, wait=wait)
        elif mod == 2:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.JUMP_XYZ, wait=wait)
        elif mod == 3:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.JUMP_MOVL_XYZ, wait=wait)
            
    def increase_xyz(self, x, y, z, r, mod, wait=False):
        if mod == 1:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.MOVL_INC, wait=wait)        
        elif mod == 0:
            self._set_ptp_cmd(x, y, z, r, mode=PTPMode.MOVJ_XYZ_INC, wait=wait)
            
    def increase_angle(self, j1, j2, j3, j4, wait=False):
        self._set_ptp_cmd(j1, j2, j3, j4, mode=PTPMode.MOVJ_INC, wait=wait)      
    
    def move_joint(self, j, wait=False):
        if j == 0:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.IDEL, wait=wait)        
        elif j == 1:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.AP_DOWN, wait=wait)        
        elif j == -1:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.AN_DOWN, wait=wait)        
        elif j == 2: 
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.BP_DOWN, wait=wait)        
        elif j == -2:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.BN_DOWN, wait=wait)        
        elif j == 3: 
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.CP_DOWN, wait=wait)        
        elif j == -3:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.CN_DOWN, wait=wait)        
        elif j == 4: 
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.DP_DOWN, wait=wait)                
        elif j == -4:
            self._set_jog_cmd(joint=0x01, mode=JOGCmd.DN_DOWN, wait=wait)        

        else: 
            print('Error!. Seleccione una articulación del 1 a 4 +/-')
            
    def move_coordinate(self, c, wait=False):
        if c == 0:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.IDEL, wait=wait)        
        elif c == 1:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.AP_DOWN, wait=wait)        
        elif c == -1:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.AN_DOWN, wait=wait)        
        elif c == 2: 
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.BP_DOWN, wait=wait)        
        elif c == -2:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.BN_DOWN, wait=wait)        
        elif c == 3: 
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.CP_DOWN, wait=wait)        
        elif c == -3:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.CN_DOWN, wait=wait)        
        elif c == 4: 
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.DP_DOWN, wait=wait)                
        elif c == -4:
            self._set_jog_cmd(joint=0x00, mode=JOGCmd.DN_DOWN, wait=wait)        

        else: 
            print('Error!. Seleccione una articulación del 1 a 4 +/-')
    
    def set_joint_params(self, v_j1, v_j2, v_j3, v_j4, a_j1, a_j2, a_j3, a_j4):
        self._set_jog_joint_params(v_j1, v_j2, v_j3, v_j4, a_j1, a_j2, a_j3, a_j4)
    
    def change_v_a_jog(self, v, a):
        self._set_jog_joint_params(v, v, v, v, a, a, a, a)
        self._set_jog_coordinate_params(v, v, v, v, a, a, a, a)

    def change_v_a_ptp(self, v, a):
        self._set_ptp_joint_params(v, v, v, v, a, a, a, a)
        
    def speed_jog(self, velocity, acceleration):
        self._set_jog_common_params(velocity, acceleration)
        self.change_v_a_jog(velocity, acceleration)
        
    def get_v_a(self):
        response = self._get_jog_joint_params()
        v_j1 = struct.unpack_from('f', response.params, 0)[0]
        v_j2 = struct.unpack_from('f', response.params, 4)[0]
        v_j3 = struct.unpack_from('f', response.params, 8)[0]
        v_j4 = struct.unpack_from('f', response.params, 12)[0]
        a_j1 = struct.unpack_from('f', response.params, 16)[0]
        a_j2 = struct.unpack_from('f', response.params, 20)[0]
        a_j3 = struct.unpack_from('f', response.params, 24)[0]
        a_j4 = struct.unpack_from('f', response.params, 28)[0]
        return v_j1, v_j2, v_j3, v_j4, a_j1, a_j2, a_j3, a_j4
            
    def home(self):
        self._set_home_cmd()

    def suck(self, enable, option):
        self._set_end_effector_suction_cup(enable, option)

    def grip(self, enable, option):
        self._set_end_effector_gripper(enable, option)

    def speed_ptp(self, velocity, acceleration):
        self._set_ptp_common_params(velocity, acceleration)
        self._set_ptp_coordinate_params(velocity, acceleration)
        self.change_v_a_ptp(velocity, acceleration)

    def wait(self, ms):
        self._set_wait_cmd(ms)

    def pose(self):
        response = self._get_pose()
        x = struct.unpack_from('f', response.params, 0)[0]
        y = struct.unpack_from('f', response.params, 4)[0]
        z = struct.unpack_from('f', response.params, 8)[0]
        r = struct.unpack_from('f', response.params, 12)[0]
        j1 = struct.unpack_from('f', response.params, 16)[0]
        j2 = struct.unpack_from('f', response.params, 20)[0]
        j3 = struct.unpack_from('f', response.params, 24)[0]
        j4 = struct.unpack_from('f', response.params, 28)[0]
        return x, y, z, r, j1, j2, j3, j4
    
    def stop(self):
        self._set_queued_cmd_force_stop_exec()

    def start_dobot(self):
        self._set_queued_cmd_start_exec()
        
