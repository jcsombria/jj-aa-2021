from serial.tools import list_ports
from pydobot import Dobot
import time
    
def move_angle(angulo, artic, device):
    j = device.pose()[3 + artic]
    
    if j > angulo:
        signo = -1
    else:
        signo = 1
    
    if signo*(angulo-j)<=25:
        v_inicial=25
        a_inicial=30
    elif (signo*(angulo-j)>25)and(signo*(angulo-j)<100):
        v_inicial=45
        a_inicial=60
    elif (signo*(angulo-j)>=100):
        v_inicial=60
        a_inicial=80

    device.speed_jog(v_inicial,a_inicial)
    device.movejoint(signo*artic, wait=False)
    fin=0
    while (fin==0):
        
#         while(v_inicial>=25 and a_inicial>=30):
#             for i in [3,2,1,0]:
#                 print('i=',i)
#                 v_inicial=v_inicial-5
#                 a_inicial=a_inicial-10
#                 device.speed_jog(v_inicial,a_inicial)
#                 time.sleep(2+i)
        
        while(v_inicial>20 and a_inicial>30):
            j1=device.pose()[3 + artic]

            if((signo*(angulo-j1))>=90):
                device.speed_jog(55,70)
            elif((signo*(angulo-j1))>=70):
                device.speed_jog(50,65)
            elif((signo*(angulo-j1))>=50):
                device.speed_jog(45,60)
            elif((signo*(angulo-j1))>=30):
                device.speed_jog(40,55)
            elif((signo*(angulo-j1))>=20):
                device.speed_jog(35,50)
            elif((signo*(angulo-j1))<=20):
                device.speed_jog(25,35)
    #         j1=device.pose()[3 + artic]
    #         if((signo*(angulo-j1))<=4):
    #             device.speed_jog(20,30)
            
                j1=device.pose()[3 + artic]
                end=0
                fin=1
                if((signo*(angulo-j1))<=10):
                  device.speed_jog(20,30)
                while(end==0):
#                     j1=device.pose()[3 + artic]
#                     if((signo*(angulo-j1))<=8):
#                         device.speed_jog(20,30)
                    j1=device.pose()[3 + artic]
                    if j1 > angulo:
                        signo = -1
                    else:
                        signo = 1
                    device.movejoint(signo*artic, wait=False)
                    if((signo*(angulo-j1))<=4):
#                         device.movejoint(0, wait=True)
                        device.speed_jog(15,25)
                        j1=device.pose()[3 + artic]
                        if((signo*(angulo-j1))<=1):
                            device.movejoint(0, wait=True)
                            end=1
                            fin=1
                            device.speed_jog(25,35)
#                           else:
#                           end=0
                        else:
                            end=0
                            fin=1
                            
                    else:
                        end=0
                        fin=1
            break

                    
            time.sleep(0.2)


        
